//
//  ViewController.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//

import UIKit
import CoreData

let kContatsURL = "http://fast-gorge.herokuapp.com/contacts"
let kAvatarURL = "http://api.adorable.io/avatar/40/"

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    var jsonResponse: [NSDictionary] = []
    
    var dataController = Data()
    var mainArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if self.dataController.fetchData(self.tableView).isEmpty {
            getUserData()
        } else {
            reloadData()
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // MARK: TableView Stack
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ContactsTableViewCell
        let thisContact = mainArray[indexPath.row] as! Contact
        
        cell.firstNameLabel.text = thisContact.firstName
        cell.surnameLabel.text = thisContact.surname
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { () -> Void in
            let image = UIImage(data: NSData(contentsOfURL: NSURL(string: kAvatarURL + thisContact.email!)!)!)
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                cell.avatarImage.image = image
            })
        }
        
        cell.accessoryType = .DisclosureIndicator
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mainArray.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("detailsSegue", sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    
    // MARK: PrepareForSegue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detailsSegue" {
            let detailsVC: DetailsViewController = segue.destinationViewController as! DetailsViewController
            let indexPath = tableView.indexPathForSelectedRow
            let thisContact = mainArray[indexPath!.row] as! Contact
            
            detailsVC.thisContact = thisContact
        }
    }
    
    // MARK: Request
    func getUserData() {

        let url: NSURL = NSURL(string: kContatsURL)!
        
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url, completionHandler: {
            (data, response, error) -> Void in
            do {
                if data == nil {
                    return
                } else {
                    let jsonMain = try NSJSONSerialization.JSONObjectWithData(data!, options: .MutableLeaves) as! NSArray
    
                    for json in jsonMain {
                        let j = json as! NSDictionary
                        self.dataController.save(j)
                    }
                    
                    self.reloadData()

                }
            } catch {
                print("Error")
            }
        })
        task.resume()
    }
    
    func reloadData() {
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.mainArray = []
            self.mainArray = self.dataController.fetchData(self.tableView)
            self.tableView.reloadData()
        }
    }
    

}

