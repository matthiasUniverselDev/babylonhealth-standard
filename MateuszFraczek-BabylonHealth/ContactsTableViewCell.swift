//
//  ContactsTableViewCell.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {

    @IBOutlet var firstNameLabel: UILabel!
    @IBOutlet var surnameLabel: UILabel!
    @IBOutlet var avatarImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
