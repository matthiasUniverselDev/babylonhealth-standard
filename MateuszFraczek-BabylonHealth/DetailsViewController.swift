//
//  DetailsViewController.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    var thisContact: Contact!

    @IBOutlet var firstNameLabel: UILabel!
    @IBOutlet var surnameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var createdAtLabel: UILabel!
    @IBOutlet var updatedAtLabel: UILabel!
    @IBOutlet var avatarImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = thisContact.firstName

        firstNameLabel.text = thisContact.firstName
        surnameLabel.text = thisContact.surname
        addressLabel.text = thisContact.address
        emailLabel.text = thisContact.email
        phoneLabel.text = thisContact.phoneNumber
        createdAtLabel.text = thisContact.createdAt
        updatedAtLabel.text = thisContact.updatedAt
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.getBigImage()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func getBigImage() {
        let bigImageURL = kAvatarURL.stringByReplacingOccurrencesOfString("40", withString: "80")
        
        if let url = NSURL(string: bigImageURL + thisContact.email!) {
            if let data = NSData(contentsOfURL: url) {
                avatarImage.image = UIImage(data: data)
            }
        }
    }
    
}
