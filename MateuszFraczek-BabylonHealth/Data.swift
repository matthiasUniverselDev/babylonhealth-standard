//
//  Data.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class Data {

    func save(json: NSDictionary) {
        
        // Saving to CoreData
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let managedObjectContext = appDelegate.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Contact", inManagedObjectContext: managedObjectContext)
        let contactItem = Contact(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
       
        if json["id"] != nil {
            let j = json["id"] as! Int
            
            let fetchRequest = NSFetchRequest(entityName: "Contact")
            let predicate = NSPredicate(format: "id == %i", j)
            fetchRequest.predicate = predicate
            
            do {
                let contact = try managedObjectContext.executeFetchRequest(fetchRequest)
                
                if contact.count != 0 {
                    print("Contact is already seaved")
                } else {
                    
                    if json["address"] != nil {
                        let address = json["address"] as? String
                        if address != nil {
                            contactItem.address = json["address"] as? String
                        } else {
                            contactItem.address = ""
                        }
                    }
                    
                    if json["createdAt"] != nil {
                        let createdAt = json["createdAt"] as? String
                        if createdAt != nil {
                            contactItem.createdAt = json["createdAt"] as? String
                            
                        } else {
                            contactItem.createdAt = ""
                        }
                    }
                    
                    if json["email"] != nil {
                        let email = json["email"] as? String
                        if email != nil {
                            contactItem.email = json["email"] as? String
                        } else {
                            contactItem.email = "Nothing"
                        }
                    }
                    
                    if json["first_name"] != nil {
                        let firstName = json["first_name"] as? String
                        if firstName != nil {
                            contactItem.firstName = json["first_name"] as? String
                            
                        } else {
                            contactItem.firstName = ""
                        }
                    }
                    
                    if json["id"] != nil {
                        let id = json["id"] as? Int
                        if id != nil {
                            contactItem.id = json["id"] as? Int
                            
                        } else {
                            contactItem.id = 0
                        }
                    }
                    
                    if json["phone_number"] != nil {
                        let phoneNumber = json["phone_number"] as? String
                        if phoneNumber != nil {
                            contactItem.phoneNumber = json["phone_number"] as? String
                            
                        } else {
                            contactItem.phoneNumber = ""
                        }
                    }
                    
                    if json["surname"] != nil {
                        let surname = json["surname"] as? String
                        if surname != nil {
                            contactItem.surname = json["surname"] as? String
                            
                        } else {
                            contactItem.surname = ""
                        }
                    }
                    
                    if json["updatedAt"] != nil {
                        let updatedAt = json["updatedAt"] as? String
                        if updatedAt != nil {
                            contactItem.updatedAt = json["updatedAt"] as? String
                            
                        } else {
                            contactItem.updatedAt = ""
                        }
                    }
                        
                    (UIApplication.sharedApplication().delegate as! AppDelegate).saveContext()
                }
            } catch {
                print("Error")
            }
        }

       
    }
    
    // Fetch From CoreData
    func fetchData(table: UITableView) -> [Contact] {
        let appDelegate = (UIApplication.sharedApplication().delegate as! AppDelegate)
        let managedObjectContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "Contact")
        
        var mainArray: [Contact] = []
        do {
            mainArray = try managedObjectContext.executeFetchRequest(fetchRequest) as! [Contact]
        } catch {
            print("Error")
        }
        
        return mainArray
    }

}