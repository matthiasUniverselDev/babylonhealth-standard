//
//  Contact+CoreDataProperties.swift
//  MateuszFraczek-BabylonHealth
//
//  Created by Mass on 03.12.2015.
//  Copyright © 2015 Mass. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Contact {

    @NSManaged var address: String?
    @NSManaged var createdAt: String?
    @NSManaged var email: String?
    @NSManaged var firstName: String?
    @NSManaged var id: NSNumber?
    @NSManaged var phoneNumber: String?
    @NSManaged var surname: String?
    @NSManaged var updatedAt: String?
    @NSManaged var avatarImage: NSData?

}
